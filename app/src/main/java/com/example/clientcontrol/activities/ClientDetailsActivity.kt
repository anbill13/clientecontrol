package com.example.clientcontrol.activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R
import com.example.clientcontrol.adapters.DirectionsAdapter
import com.example.clientcontrol.dataclass.Client
import com.example.clientcontrol.dataclass.Direction
import com.example.clientcontrol.interfacebutton.ClickListener
import com.example.clientcontrol.viewmodel.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_direcctions.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class ClientDetailsActivity : AppCompatActivity() {

    private lateinit var adapter: DirectionsAdapter
    private val viewModel by lazy { ViewModelProvider(this).get(MainViewModel::class.java) }
    private var directions: MutableList<Direction> = mutableListOf()
    var clientAddress: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_direcctions)

        directionsAdd!!.isEnabled = false

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        )
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION), 111)
        else
            directionsAdd!!.isEnabled = true

        val back: ImageView = findViewById(R.id.directionsBack)

        back.setOnClickListener {
            this.finish()
        }

        val nameCompany: TextView = findViewById(R.id.directionsTitle)

        val client = intent.getSerializableExtra(CLIENT_LIST) as Client
        val companyId = intent.getStringExtra(COMPANY_LIST)
        nameCompany.text = "${client.name}'s"
        directions = client.directions

        val recyclerView: RecyclerView = findViewById(R.id.recyclerViewDirections)

        viewModel.fetchDirectionsData(client.id).observe(this, {

            adapter = DirectionsAdapter(directions, client.logo, object : ClickListener {

                override fun onClick(view: View, position: Int) {

                    chooseMap(
                        this@ClientDetailsActivity,
                        directions[position].longitude,
                        directions[position].latitude
                    )
                }

                override fun onLongClick(v: View?, position: Int): Boolean {

                    Toast.makeText(this@ClientDetailsActivity, "MESSAGE", Toast.LENGTH_SHORT).show()

                    return true
                }

            })

            recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            recyclerView.adapter = adapter
            recyclerView.setHasFixedSize(true)
        })

        directionsAdd.setOnClickListener {

            val dialog = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)

            val view = layoutInflater.inflate(
                R.layout.bottom_sheet_dialog_client_details,
                findViewById<LinearLayout>(R.id.bottom_sheet_client_details)
            )

            saveCompany = view.findViewById(R.id.addSaveClientDetails)
            clientAddress = view.findViewById(R.id.addressClientDetails)
            val clientCity: EditText = view.findViewById(R.id.cityClientDetails)
            val clientCountry: EditText = view.findViewById(R.id.addClientDetailsCountry)
            val clientLatitude: EditText = view.findViewById(R.id.addClientDetailsLatitude)
            val clientLongitude: EditText = view.findViewById(R.id.addClientDetailsLongitude)

            saveCompany!!.setOnClickListener {
                val address = clientAddress!!.text.toString()
                val city = clientCity.text.toString()
                val country = clientCountry.text.toString()
                val logo = DIRECTIONS_LOGO


                val gc = Geocoder(this,Locale.getDefault())
                val addresses = gc.getFromLocationName(address,2)
                val addreess = addresses[0]

                clientLatitude.setText(addreess.latitude.toString())
                clientLongitude.setText(addreess.longitude.toString())
                clientCity.setText("Santo Domingo")

                val latitude: String = addreess.longitude.toString()
                val longitude: String = addreess.latitude.toString()

                if (address == "" || country == "") {

                    Snackbar.make(
                        dialog.window!!.decorView,
                        recyclerView.context.getString(R.string.formEmpty),
                        Snackbar.LENGTH_SHORT
                    ).show()

                    return@setOnClickListener
                }
                val directionAdd = Direction(address, city, country, latitude, logo, longitude)
                directions.add(directionAdd)

                viewModel.saveDirectionsData(companyId!!, client.id, directions).observe(this, {
                    if (it) {
                        adapter.notifyDataSetChanged()
                        dialog.dismiss()
                    } else {
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                })
            }

            dialog.setContentView(view)
            dialog.show()

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 111 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            saveCompany!!.isEnabled = true
        }
    }


    private fun chooseMap(context: Context, latitude: String, longitude: String) {

        val url = "waze://?ll=$latitude,$longitude&navigate=yes"
        val intentWaze = Intent(Intent.ACTION_VIEW, Uri.parse(url)).setPackage("com.waze")

        val uriGoogle = "google.navigation:q=$latitude,$longitude"
        val intentGoogleNav =
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(uriGoogle)
            ).setPackage("com.google.android.apps.maps")

        val title: String = context.getString(R.string.app_name)
        val chooserIntent = Intent.createChooser(intentGoogleNav, title)
        val arr = arrayOfNulls<Intent>(1)
        arr[0] = intentWaze
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arr)
        context.startActivity(chooserIntent)

    }

    companion object {
        const val CLIENT_LIST = "selectedClient"
        const val COMPANY_LIST = "selectedCompanyAndClient"
        const val DIRECTIONS_LOGO = "https://image.flaticon.com/icons/png/512/355/355980.png"
        var saveCompany: FloatingActionButton?= null

    }
}

