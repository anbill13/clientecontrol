package com.example.clientcontrol.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R
import com.example.clientcontrol.adapters.ClientAdapter
import com.example.clientcontrol.controllerSwipe.CompanyDetailsSwipes
import com.example.clientcontrol.controllerSwipe.ControllerSwipe
import com.example.clientcontrol.dataclass.Client
import com.example.clientcontrol.dataclass.ClientDocument
import com.example.clientcontrol.dataclass.Company
import com.example.clientcontrol.interfacebutton.ClickListener
import com.example.clientcontrol.viewmodel.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_client.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_sheet_dialog_company_details.*
import kotlinx.android.synthetic.main.bottom_sheet_dialog_company_details.view.*


class CompanyDetailActivity : AppCompatActivity() {

    private lateinit var adapter: ClientAdapter
    private val viewModel by lazy { ViewModelProvider(this).get(MainViewModel::class.java) }
    private var clientList: MutableList<Client> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client)

        val back: ImageView = findViewById(R.id.clientBack)

        back.setOnClickListener {

            this.finish()
        }

        val bundle = intent.extras!!
        val company = bundle.getParcelable<Company>("selectedCompany")
        val nameCompany: TextView = findViewById(R.id.clientName)
        nameCompany.text = "${company!!.owner}'s"

        val recyclerView: RecyclerView = findViewById(R.id.recyclerViewClient)

        adapter = ClientAdapter(clientList, company.logo, object : ClickListener {

            override fun onClick(view: View, position: Int) {

                val clientList = clientList[position]
                val intent = Intent(this@CompanyDetailActivity, ClientDetailsActivity::class.java)
                intent.putExtra(ClientDetailsActivity.CLIENT_LIST, clientList)
                intent.putExtra(ClientDetailsActivity.COMPANY_LIST, company.id)

                startActivity(intent)
                return
            }

            override fun onLongClick(v: View?, position: Int): Boolean {

                Toast.makeText(this@CompanyDetailActivity, "MESSAGE", Toast.LENGTH_SHORT).show()
                return true
            }

        })

        val swipeController = ControllerSwipe(
            this,
            CompanyDetailsSwipes(this, recyclerView, viewModel, clientList, company)
        )
        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(recyclerView)


        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)
        adapter.notifyDataSetChanged()

        fetchCompanies(company)

        clientFloatingActionButton.setOnClickListener {

            val dialog = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)

            val view = layoutInflater.inflate(
                R.layout.bottom_sheet_dialog_company_details,
                findViewById<LinearLayout>(R.id.bottom_sheet_company_details)
            )

            val saveCompany: FloatingActionButton = view.findViewById(R.id.addSaveClient)
            val clientName: EditText = view.findViewById(R.id.addClientName)
            val clientLastName: EditText = view.findViewById(R.id.addClientLastName)
            val clientPhone: EditText = view.findViewById(R.id.addClientPhone)

            val radioMan: RadioButton = view.findViewById(R.id.radioMan)
            val radioWoman: RadioButton = view.findViewById(R.id.radioWoman)

            var logo = ""

            radioMan.setOnClickListener { logo = LOGO_MAN }
            radioWoman.setOnClickListener { logo = LOGO_WOMAN }

            saveCompany.setOnClickListener {

                val name = clientName.text.toString().trim()
                val lastName = clientLastName.text.toString().trim()
                val phone = clientPhone.text.toString().trim()

                if ( name == "" || lastName == "" || phone == "" ) {

                    Snackbar.make(
                        dialog.window!!.decorView,
                        recyclerView.context.getString(R.string.formEmpty),
                        Snackbar.LENGTH_SHORT
                    ).show()

                    return@setOnClickListener
                }
                val client = ClientDocument(lastName, logo, name, phone)

                viewModel.saveClient(company.id, client).observe(this, {
                    if (it == true) {

                        fetchCompanies(company)
                        dialog.dismiss()

                    } else {
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                })
            }

            dialog.setContentView(view)
            dialog.show()

        }

    }

    fun fetchCompanies(company: Company) {

        clientList.clear()

        viewModel.fetchClientData(company.id).observe(this, {

            clientList.addAll(it.toList())
            adapter.notifyDataSetChanged()
        })
    }

    companion object {
        const val LOGO_MAN = "https://image.flaticon.com/icons/png/512/3135/3135715.png"
        const val LOGO_WOMAN = "https://image.flaticon.com/icons/png/512/4305/4305696.png"
    }
}