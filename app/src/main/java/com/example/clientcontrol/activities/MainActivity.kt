package com.example.clientcontrol.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R
import com.example.clientcontrol.adapters.CompanyListAdapter
import com.example.clientcontrol.adapters.IconCompaniesAdapter
import com.example.clientcontrol.controllerSwipe.ControllerSwipe
import com.example.clientcontrol.controllerSwipe.EditSwipe
import com.example.clientcontrol.dataclass.Company
import com.example.clientcontrol.dataclass.CompanyDocument
import com.example.clientcontrol.dataclass.IconComp
import com.example.clientcontrol.interfacebutton.ClickListener
import com.example.clientcontrol.viewmodel.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

open class MainActivity : AppCompatActivity(), LifecycleOwner {
    private var adapter: CompanyListAdapter? = null
    private var adapterIcon: IconCompaniesAdapter? = null
    private var recyclerView: RecyclerView? = null
    private lateinit var currentIconComp: String
    private lateinit var currentIconName: String
    private val viewModel by lazy { ViewModelProvider(this).get(MainViewModel::class.java) }
    private var companyList: MutableList<Company> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = CompanyListAdapter(companyList, object : ClickListener {

            override fun onClick(view: View, position: Int) {

                val intent = Intent(this@MainActivity, CompanyDetailActivity::class.java)
                val bundle = Bundle()
                bundle.putParcelable("selectedCompany", companyList[position])
                intent.putExtras(bundle)
                startActivity(intent)

            }

            override fun onLongClick(v: View?, position: Int): Boolean {

                Toast.makeText(this@MainActivity, "MESSAGE", Toast.LENGTH_SHORT).show()

                return true
            }

        })

        val swipeController =
            ControllerSwipe(this, EditSwipe(this, recyclerViewCompanyLis, viewModel, companyList))
        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(recyclerViewCompanyLis)

        recyclerView = findViewById(R.id.recyclerViewCompanyLis)
        recyclerView!!.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView!!.adapter = adapter
        recyclerView!!.setHasFixedSize(true)

        fetchCompanies()

        companyFloatingActionButton.setOnClickListener {

            val dialog = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
            val view = layoutInflater.inflate(
                R.layout.bottom_sheet_dialog,
                findViewById<LinearLayout>(R.id.bottom_sheet_company_add)
            )

            val saveCompany: FloatingActionButton = view.findViewById(R.id.saveCompany)
            val companyLogo: EditText = view.findViewById(R.id.companyLogo)
            val companyName: EditText = view.findViewById(R.id.companyNameFrom)
            val companyOwner: EditText = view.findViewById(R.id.companyOwnerApp)
            val companyPhone: EditText = view.findViewById(R.id.companyPhone)
            companyName.isEnabled = false

            adapterIcon = IconCompaniesAdapter(icons(), object : ClickListener {

                override fun onClick(view: View, position: Int) {

                    currentIconComp = icons()[position].logo
                    currentIconName = icons()[position].name
                    companyLogo.setText(currentIconComp)
                    companyName.setText(currentIconName)

                }

                override fun onLongClick(v: View?, position: Int): Boolean {
                    return false
                }
            })


            val recyclerViewIcon: RecyclerView = view.findViewById(R.id.iconCompany)
            recyclerViewIcon.layoutManager =
                LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
            recyclerViewIcon.adapter = adapterIcon
            recyclerViewIcon.setHasFixedSize(true)

            saveCompany.setOnClickListener {
                var logo = companyLogo.text.toString()
                val name = companyName.text.toString()
                val owner = companyOwner.text.toString()
                val phone = companyPhone.text.toString()

                if (logo == "" || name == "" || owner == "" || phone == "") {

                    Snackbar.make(
                        dialog.window!!.decorView,
                        view.context.getString(R.string.formEmpty),
                        Snackbar.LENGTH_SHORT
                    ).show()

                    return@setOnClickListener
                }
                logo = currentIconComp

                val company = CompanyDocument(logo, name, phone, owner)

                viewModel.saveCompany(company).observe(this, {
                    if (it == true) {

                        fetchCompanies()
                        Toast.makeText(this, "Susses", Toast.LENGTH_SHORT).show()
                        dialog.dismiss()

                    } else {
                        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                    }
                })
            }

            dialog.setContentView(view)
            dialog.show()

        }
    }

    fun icons(): MutableList<IconComp> {
        val companies: MutableList<IconComp> = ArrayList()
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/2702/2702602.png", "Google"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/731/731985.png", "Apple"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/733/733579.png", "Twitter"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/356/356001.png", "Twitch"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/906/906361.png", "Discord"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/1/1443.png", "Sony"))
        return companies
    }

    fun fetchCompanies() {

        companyList.clear()

        viewModel.fetCompanyData().observe(this, {

            shimmerFrameLayout.isShimmerStarted
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility = View.GONE

            companyList.addAll(it.toList())
            adapter!!.notifyDataSetChanged()
        })
    }

}
