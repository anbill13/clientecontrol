package com.example.clientcontrol.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.clientcontrol.R
import com.example.clientcontrol.dataclass.Client
import com.example.clientcontrol.holders.ClientViewHolder
import com.example.clientcontrol.interfacebutton.ClickListener

class ClientAdapter(
    private var clientList: MutableList<Client>,
    private var companyLogo: String,
    private val clickListener: ClickListener
) : RecyclerView.Adapter<ClientViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(
            R.layout.item_row_client,
            parent, false
        )
        return ClientViewHolder(
            itemView = layout,
            listener = clickListener
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ClientViewHolder, position: Int) {
        val client = clientList[position]

        Glide.with(holder.itemView).load(client.logo).into(holder.clientCircle)
        Glide.with(holder.itemView).load(companyLogo).into(holder.companyCircle)
        holder.name.text = client.name
        holder.lastName.text = client.lastName
        holder.directions.text = "Directions:  ${client.directions.size}"

    }

    override fun getItemCount(): Int = clientList.size

}
