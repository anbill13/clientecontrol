package com.example.clientcontrol.adapters

import android.view.LayoutInflater
import android.view.View.inflate
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.clientcontrol.R
import com.example.clientcontrol.dataclass.Company
import com.example.clientcontrol.holders.CompanyViewHolder
import com.example.clientcontrol.interfacebutton.ClickListener

class CompanyListAdapter(
    private var listCompany: MutableList<Company>,
    private val clickListener: ClickListener
) : RecyclerView.Adapter<CompanyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(
            R.layout.item_row_company_list,
            parent, false
        )
        return CompanyViewHolder(
            itemView = layout,
            listener = clickListener
        )
    }

    override fun onBindViewHolder(holder: CompanyViewHolder, position: Int) {
        val company = listCompany[position]

        Glide.with(holder.itemView).load(company.logo).into(holder.companyImage)
        holder.name.text = company.name
        holder.phone.text = company.phone
        holder.owner.text = company.owner
        holder.setIsRecyclable(true)

    }

    override fun getItemCount(): Int = listCompany.size

}
