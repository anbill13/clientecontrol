package com.example.clientcontrol.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.clientcontrol.R
import com.example.clientcontrol.dataclass.Direction
import com.example.clientcontrol.holders.DirectionsViewHolder
import com.example.clientcontrol.interfacebutton.ClickListener

class DirectionsAdapter(
    private var directionsList: MutableList<Direction>,
    private val clientLogo: String,
    private val clickListener: ClickListener
) : RecyclerView.Adapter<DirectionsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DirectionsViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(
            R.layout.item_row_directions,
            parent, false
        )
        return DirectionsViewHolder(
            itemView = layout,
            listener = clickListener
        )
    }

    override fun onBindViewHolder(holder: DirectionsViewHolder, position: Int) {
        val direction = directionsList[position]

        Glide.with(holder.itemView).load(direction.logo).into(holder.directionsLogo)
        Glide.with(holder.itemView).load(clientLogo).into(holder.clientCircle)
        holder.address.text = direction.address
        holder.city.text = direction.city
        holder.country.text = direction.country

    }

    override fun getItemCount(): Int = directionsList.size

}
