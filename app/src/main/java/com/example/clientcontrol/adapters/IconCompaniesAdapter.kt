package com.example.clientcontrol.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.clientcontrol.R
import com.example.clientcontrol.dataclass.IconComp
import com.example.clientcontrol.holders.IconViewHolder
import com.example.clientcontrol.interfacebutton.ClickListener

class IconCompaniesAdapter(
    private var iconComp: MutableList<IconComp>,
    private val clickListener: ClickListener
) : RecyclerView.Adapter<IconViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IconViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(
            R.layout.item_row_companies_icon,
            parent, false
        )
        return IconViewHolder(
            itemView = layout,
            listener = clickListener
        )
    }

    override fun onBindViewHolder(holder: IconViewHolder, position: Int) {
        val icon = iconComp[position]
        Glide.with(holder.itemView).load(icon.logo).into(holder.companyImage)

    }

    override fun getItemCount(): Int = iconComp.size

}
