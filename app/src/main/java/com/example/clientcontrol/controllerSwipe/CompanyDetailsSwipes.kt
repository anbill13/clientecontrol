package com.example.clientcontrol.controllerSwipe

import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R
import com.example.clientcontrol.activities.CompanyDetailActivity
import com.example.clientcontrol.dataclass.Client
import com.example.clientcontrol.dataclass.ClientDocument
import com.example.clientcontrol.dataclass.Company
import com.example.clientcontrol.viewmodel.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar


class CompanyDetailsSwipes(
    private val context: CompanyDetailActivity,
    private val recyclerView: RecyclerView,
    private val viewModel: MainViewModel,
    private var clientList: MutableList<Client> = mutableListOf(),
    private var company: Company? = null


) :
    ControllerSwipe.ButtonCallbacks {

    override fun onEditButtonClick(position: Int, view: View) {
        val dialog = BottomSheetDialog(recyclerView.context, R.style.BottomSheetDialogTheme)
        val dialogView = dialog.layoutInflater.inflate(
            R.layout.bottom_sheet_dialog_company_details_update,
            recyclerView.findViewById<LinearLayout>(R.id.bottom_sheet_company_details_update)
        )

        val saveCompany: FloatingActionButton = dialogView.findViewById(R.id.updateCompanyDetails)
        val companyDetailsLogo: EditText = dialogView.findViewById(R.id.companyDetailsLogoUpdate)
        val companyDetailsName: EditText =
            dialogView.findViewById(R.id.companyDetailsNameFromUpdate)
        val companyDetailsLastName: EditText =
            dialogView.findViewById(R.id.companyDetailsLastNameAppUpdate)
        val companyDetailsPhone: EditText = dialogView.findViewById(R.id.companyDetailsPhoneUpdate)

        companyDetailsLogo.setText(clientList[position].logo)
        companyDetailsName.setText(clientList[position].name)
        companyDetailsLastName.setText(clientList[position].lastName)
        companyDetailsPhone.setText(clientList[position].phoneNumber)


        saveCompany.setOnClickListener {

            val logo = companyDetailsLogo.text.toString().trim()
            val name = companyDetailsName.text.toString().trim()
            val lastName = companyDetailsLastName.text.toString().trim()
            val phone = companyDetailsPhone.text.toString().trim()

            if (name == "" || lastName == "" || phone == "") {

                Snackbar.make(
                    dialog.window!!.decorView,
                    recyclerView.context.getString(R.string.formEmpty),
                    Snackbar.LENGTH_SHORT
                ).show()

                return@setOnClickListener
            }
            val clientDocument = ClientDocument(lastName, logo, name, phone)

            viewModel.updateClient(company!!.id, clientList[position].id, clientDocument)
                .observe(context, {

                    context.fetchCompanies(company!!)
                })
            dialog.dismiss()
        }

        dialog.setContentView(dialogView)
        dialog.show()

        recyclerView.adapter?.notifyItemChanged(position)

    }

    override fun onDeleteButtonClick(position: Int, view: View) {

        val dialog = AlertDialog.Builder(view.context)
            .setTitle(R.string.warning)
            .setMessage(R.string.deleteRegister)
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.accept) { dialog, _ ->
                viewModel.deleteClient(company!!.id, clientList[position].id)
                context.fetchCompanies(company!!)
                dialog.dismiss()
            }
            .setCancelable(false)
            .create()

        dialog.show()


        recyclerView.adapter?.notifyItemChanged(position)

    }
}