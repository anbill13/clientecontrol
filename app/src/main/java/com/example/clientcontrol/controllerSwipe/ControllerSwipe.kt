package com.example.clientcontrol.controllerSwipe
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.LEFT
import androidx.recyclerview.widget.ItemTouchHelper.RIGHT
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R

class ControllerSwipe(context: Context, private val callbacks: ButtonCallbacks) : ItemTouchHelper.Callback() {

    private var mSwipeDirection: Int = 0

    private val mEditPaint by lazy {
        val paint = Paint()
        paint.color = ContextCompat.getColor(context, R.color.edit)
        paint
    }

    private val mEditDrawable by lazy { ContextCompat.getDrawable(context, R.drawable.ic_round_edit) }

    private val mRemovePaint by lazy {
        val paint = Paint()
        paint.color = ContextCompat.getColor(context, R.color.delete)
        paint
    }
    private val mRemoveDrawable by lazy { ContextCompat.getDrawable(context, R.drawable.ic_round_delete) }

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        return makeMovementFlags(0, LEFT or RIGHT)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        if (direction == LEFT) {
            callbacks.onEditButtonClick(viewHolder.adapterPosition, viewHolder.itemView)
        } else if (direction == RIGHT) {
            callbacks.onDeleteButtonClick(viewHolder.adapterPosition, viewHolder.itemView)
        }

    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (dX > 0) {
            mSwipeDirection = RIGHT
            val view = viewHolder.itemView
            val rect = RectF(0f, view.top + 14f, dX.toInt() + 32f, view.bottom - 13f)
            c.drawRoundRect(rect, 8f, 8f, mRemovePaint)

            val iconRect = RectF(rect)
            iconRect.right = rect.left + 128f
            mRemoveDrawable.drawCentered(c, iconRect)
        } else if (dX < 0) {
            mSwipeDirection = LEFT
            val view = viewHolder.itemView
            val rect = RectF(dX.toInt() - 32f, view.top + 14f, view.right.toFloat(), view.bottom - 13f)
            c.drawRoundRect(rect, 8f, 8f, mEditPaint)

            val iconRect = RectF(rect)
            iconRect.left = rect.right - 128f
            mEditDrawable.drawCentered(c, iconRect)
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    companion object {
        private fun Drawable?.drawCentered(canvas: Canvas, rectF: RectF) {
            this ?: return

            val iconWidth = intrinsicWidth
            val iconHeight = intrinsicHeight
            val rectHalfWidth = rectF.width().toInt() / 2
            val rectHalfHeight = rectF.height().toInt() / 2

            val left = rectF.left.toInt() + (rectHalfWidth - (iconWidth / 2))
            val top = rectF.top.toInt() + (rectHalfHeight - (iconHeight / 2))

            bounds = Rect(
                left,
                top,
                left + iconWidth,
                top + iconHeight
            )
            draw(canvas)
        }
    }

    interface ButtonCallbacks {

        fun onEditButtonClick(position: Int, view: View)
        fun onDeleteButtonClick(position: Int, view: View)

    }
}

