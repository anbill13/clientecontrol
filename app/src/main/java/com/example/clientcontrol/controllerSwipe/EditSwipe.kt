package com.example.clientcontrol.controllerSwipe

import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R
import com.example.clientcontrol.activities.MainActivity
import com.example.clientcontrol.adapters.IconCompaniesAdapter
import com.example.clientcontrol.dataclass.Company
import com.example.clientcontrol.dataclass.CompanyDocument
import com.example.clientcontrol.dataclass.IconComp
import com.example.clientcontrol.interfacebutton.ClickListener
import com.example.clientcontrol.viewmodel.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar


class EditSwipe(
    private val context: MainActivity,
    private var recyclerView: RecyclerView,
    private val viewModel: MainViewModel,
    private var companyList: MutableList<Company> = mutableListOf(),
) :
    ControllerSwipe.ButtonCallbacks {

    private lateinit var adapterIcon: IconCompaniesAdapter
    private lateinit var currentIconComp: String
    private lateinit var currentIconName: String

    override fun onEditButtonClick(position: Int, view: View) {
        val dialog = BottomSheetDialog(recyclerView.context, R.style.BottomSheetDialogTheme)
        val dialogView = dialog.layoutInflater.inflate(
            R.layout.bottom_sheet_dialog_company_update,
            recyclerView.findViewById<LinearLayout>(R.id.bottom_sheet_company_update)
        )

        val saveCompany: FloatingActionButton = dialogView.findViewById(R.id.updateCompany)
        val companyLogo: EditText = dialogView.findViewById(R.id.companyLogoUpdate)
        val companyName: EditText = dialogView.findViewById(R.id.companyNameFromUpdate)
        val companyOwner: EditText = dialogView.findViewById(R.id.companyOwnerAppUpdate)
        val companyPhone: EditText = dialogView.findViewById(R.id.companyPhoneUpdate)

        companyLogo.setText(companyList[position].logo)
        companyName.setText(companyList[position].name)
        companyOwner.setText(companyList[position].owner)
        companyPhone.setText(companyList[position].phone)
        companyName.isEnabled = false

        adapterIcon = IconCompaniesAdapter(icons(), object : ClickListener {

            override fun onClick(view: View, position: Int) {

                currentIconComp = icons()[position].logo
                currentIconName = icons()[position].name
                companyLogo.setText(currentIconComp)
                companyName.setText(currentIconName)

            }

            override fun onLongClick(v: View?, position: Int): Boolean {
                return false
            }
        })

        val recyclerViewIcon: RecyclerView = dialogView.findViewById(R.id.iconCompany)
        recyclerViewIcon.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        recyclerViewIcon.adapter = adapterIcon
        recyclerViewIcon.setHasFixedSize(true)

        saveCompany.setOnClickListener {

            val logo = companyLogo.text.toString().trim()
            val name = companyName.text.toString().trim()
            val owner = companyOwner.text.toString().trim()
            val phone = companyPhone.text.toString().trim()

            if (logo == "" || name == "" || owner == "" || phone == "") {

                Snackbar.make(
                    dialog.window!!.decorView,
                    recyclerView.context.getString(R.string.formEmpty),
                    Snackbar.LENGTH_SHORT
                ).show()

                return@setOnClickListener
            }
            val company = CompanyDocument(logo, name, phone, owner)

            viewModel.updateCompany(companyList[position].id, company).observe(context, {

                context.fetchCompanies()
            })
            dialog.dismiss()
        }

        dialog.setContentView(dialogView)
        dialog.show()

        recyclerView.adapter?.notifyItemChanged(position)

    }

    override fun onDeleteButtonClick(position: Int, view: View) {

        val dialog = AlertDialog.Builder(view.context)
            .setTitle(R.string.warning)
            .setMessage(R.string.deleteRegister)
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.accept) { dialog, _ ->
                viewModel.deleteCompany(companyList[position].id)
                context.fetchCompanies()
                dialog.dismiss()
            }
            .setCancelable(false)
            .create()

        dialog.show()


        recyclerView.adapter?.notifyItemChanged(position)

    }

    fun icons(): MutableList<IconComp> {
        val companies: MutableList<IconComp> = ArrayList()
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/2702/2702602.png", "Google"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/731/731985.png", "Apple"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/733/733579.png", "Twitter"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/356/356001.png", "Twitch"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/906/906361.png", "Discord"))
        companies.add(IconComp("https://cdn-icons-png.flaticon.com/512/1/1443.png", "Sony"))
        return companies
    }
}