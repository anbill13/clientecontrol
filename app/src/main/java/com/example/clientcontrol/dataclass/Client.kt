package com.example.clientcontrol.dataclass


import java.io.Serializable


data class Client(
    var id: String = "",
    var lastName: String = "",
    var logo: String = "",
    var name: String = "",
    var phoneNumber: String = "",
    var directions: MutableList<Direction> = mutableListOf()
) : Serializable


data class Direction(
    var address: String = "",
    var city: String = "",
    var country: String = "",
    var latitude:String ="",
    var logo: String = "",
    var longitude:String =""

    ) : Serializable

data class ClientDocument(
    var lastName: String = "",
    var logo: String = "",
    var name: String = "",
    var phoneNumber: String = "",
    var directions: MutableList<Direction> = mutableListOf()
): Serializable