package com.example.clientcontrol.dataclass

import android.os.Parcel
import android.os.Parcelable


data class Company(
    val id: String ="",
    var logo: String ="",
    var name: String ="",
    var phone: String ="",
    var owner: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!

    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(logo)
        parcel.writeString(name)
        parcel.writeString(phone)
        parcel.writeString(owner)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Company> {
        override fun createFromParcel(parcel: Parcel): Company {
            return Company(parcel)
        }

        override fun newArray(size: Int): Array<Company?> {
            return arrayOfNulls(size)
        }
    }
}

data class CompanyDocument(
    var logo: String ="",
    var name: String ="",
    var phone: String ="",
    var owner: String = ""
)
