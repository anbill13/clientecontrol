package com.example.clientcontrol.domain.data.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.clientcontrol.dataclass.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions

class StoreRepository {
    private val db = FirebaseFirestore.getInstance()

    fun getCompanies(): LiveData<MutableList<Company>> {
        val companyMutableList = MutableLiveData<MutableList<Company>>()

        FirebaseFirestore.getInstance().collection("companies").get().addOnSuccessListener {
            val companyList = mutableListOf<Company>()

            for (document in it) {
                val companyId = document.id
                val name = document.getString("name")
                val logo = document.getString("logo")
                val phone = document.getString("phone")
                val owner = document.getString("owner")

                val company = Company(companyId, logo!!, name!!, phone!!, owner!!)

                companyList.add(company)
            }
            companyMutableList.value = companyList
        }

        return  companyMutableList
    }
    fun saveCompany(company: CompanyDocument): MutableLiveData<Boolean>{
        val successMutableLiveData = MutableLiveData<Boolean>()

        db.collection("companies")
            .document()
            .set(company)
            .addOnSuccessListener { successMutableLiveData.value = true }
            .addOnFailureListener {
                successMutableLiveData.value = false
            }

        return successMutableLiveData
    }

    fun updateCompany(
        companyId: String,
        company: CompanyDocument
    ): LiveData<MutableList<CompanyDocument>> {
        val companyMutableList = MutableLiveData<MutableList<CompanyDocument>>()
        db.collection("companies")
            .document(companyId)
            .set(company).addOnSuccessListener {
                val companyList = mutableListOf<CompanyDocument>()

                companyMutableList.value = companyList
            }

        return companyMutableList
    }

    fun deleteCompany(companyId: String): MutableLiveData<Boolean> {
        val successMutableLiveData = MutableLiveData<Boolean>()

        db.collection("companies").document(companyId).delete()
            .addOnSuccessListener {
                successMutableLiveData.value = true
            }
            .addOnFailureListener {
                successMutableLiveData.value = false
            }

        return successMutableLiveData
    }
    fun getClients(companyId: String): LiveData<MutableList<Client>> {
        val clientMutableList = MutableLiveData<MutableList<Client>>()

        db.collection("companies")
            .document(companyId)
            .collection("clients").get().addOnSuccessListener {
                val clientList = mutableListOf<Client>()
                for (clientDocument in it) {
                    val client: Client = clientDocument.toObject(Client::class.java)
                    client.id = clientDocument.id
                    client.directions
                    clientList.add(client)
                }
                clientMutableList.value = clientList
            }

        return clientMutableList
    }

    fun saveClient(companyId: String, client: ClientDocument): MutableLiveData<Boolean> {
        val successMutableLiveData = MutableLiveData<Boolean>()

        db.collection("companies")
            .document(companyId)
            .collection("clients")
            .document()
            .set(client).addOnSuccessListener { successMutableLiveData.value = true }
            .addOnFailureListener {
                successMutableLiveData.value = false
            }

        return successMutableLiveData
    }

    fun updateClient(companyId: String, clientId: String, clientDocument: ClientDocument):
            LiveData<MutableList<ClientDocument>> {
        val clientMutableList = MutableLiveData<MutableList<ClientDocument>>()
        db.collection("companies")
            .document(companyId)
            .collection("clients")
            .document(clientId)
            .set(clientDocument).addOnSuccessListener {
                val companyList = mutableListOf<ClientDocument>()
                clientMutableList.value = companyList
            }

        return clientMutableList
    }

    fun deleteClient(companyId: String, clientId: String): MutableLiveData<Boolean> {
        val successMutableLiveData = MutableLiveData<Boolean>()

        db.collection("companies")
            .document(companyId)
            .collection("clients")
            .document(clientId).delete()
            .addOnSuccessListener {
                successMutableLiveData.value = true
            }
            .addOnFailureListener {
                successMutableLiveData.value = false
            }

        return successMutableLiveData
    }

    fun getDirections(clientId: String): LiveData<MutableList<Direction>> {
        val directionMutableList = MutableLiveData<MutableList<Direction>>()
        db.collection("clients")
            .document(clientId)
            .collection("directions").get().addOnSuccessListener {
                val directionDetailsList = mutableListOf<Direction>()
                for (directionDocument in it) {
                    val directionDetails: Direction =
                        directionDocument.toObject(Direction::class.java)
                    directionDetailsList.add(directionDetails)
                }
                directionMutableList.value = directionDetailsList
            }

        return directionMutableList
    }

    fun saveDirectionsDetails(
        companyId: String, clientId: String, direction: MutableList<Direction>
    ): MutableLiveData<Boolean> {

        val successLivedata = MutableLiveData<Boolean>()
        val hashObject: MutableMap<String, MutableList<Direction>> = HashMap()

        hashObject["directions"] = direction

        db.collection("companies")
            .document(companyId)
            .collection("clients")
            .document(clientId).set(hashObject, SetOptions.merge())
            .addOnSuccessListener {
                successLivedata.value = true
            }
            .addOnFailureListener {
                successLivedata.value = false
            }

        return successLivedata;
    }

}