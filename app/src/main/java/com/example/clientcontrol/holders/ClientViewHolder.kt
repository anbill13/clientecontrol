package com.example.clientcontrol.holders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R
import com.example.clientcontrol.interfacebutton.ClickListener
import de.hdodenhof.circleimageview.CircleImageView

class ClientViewHolder(itemView: View, listener: ClickListener) :
    RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {

    val name: TextView = itemView.findViewById(R.id.clientName)
    val lastName: TextView = itemView.findViewById(R.id.clientLastName)
    val clientCircle: CircleImageView = itemView.findViewById(R.id.clientCircle)
    val companyCircle: CircleImageView = itemView.findViewById(R.id.companyCircle)
    val directions: TextView = itemView.findViewById(R.id.numbersAddress)


    private val clickListener = listener

    init {
        itemView.setOnLongClickListener(this)
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        clickListener.onClick(v!!, adapterPosition)
    }

    override fun onLongClick(v: View?): Boolean {

        clickListener.onLongClick(v!!, adapterPosition)
        return true
    }
}