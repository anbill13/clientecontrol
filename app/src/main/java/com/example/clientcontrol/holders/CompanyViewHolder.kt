package com.example.clientcontrol.holders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R
import com.example.clientcontrol.interfacebutton.ClickListener
import de.hdodenhof.circleimageview.CircleImageView

class CompanyViewHolder(itemView: View, listener: ClickListener) :
    RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {

    val companyImage: CircleImageView = itemView.findViewById(R.id.companyImage)
    val name: TextView = itemView.findViewById(R.id.companyName)
    val phone: TextView = itemView.findViewById(R.id.companyPhone)
    val owner: TextView = itemView.findViewById(R.id.companyOwner)


    private val clickListener = listener

    init {
        itemView.setOnLongClickListener(this)
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        clickListener.onClick(v!!, adapterPosition)
    }

    override fun onLongClick(v: View?): Boolean {

        clickListener.onLongClick(v!!, adapterPosition)
        return true
    }
}