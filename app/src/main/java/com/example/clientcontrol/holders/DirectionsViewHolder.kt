package com.example.clientcontrol.holders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R
import com.example.clientcontrol.interfacebutton.ClickListener
import de.hdodenhof.circleimageview.CircleImageView

class DirectionsViewHolder(itemView: View, listener: ClickListener) :
    RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {

    val directionsLogo: CircleImageView = itemView.findViewById(R.id.directionsCircle)
    val clientCircle: CircleImageView = itemView.findViewById(R.id.subClientCircle)

    val address: TextView = itemView.findViewById(R.id.directionsAddress)
    val city: TextView = itemView.findViewById(R.id.directionsCity)
    val country: TextView = itemView.findViewById(R.id.directionsCountry)


    private val clickListener = listener

    init {
        itemView.setOnLongClickListener(this)
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        clickListener.onClick(v!!, adapterPosition)
    }

    override fun onLongClick(v: View?): Boolean {

        clickListener.onLongClick(v!!, adapterPosition)
        return true
    }
}