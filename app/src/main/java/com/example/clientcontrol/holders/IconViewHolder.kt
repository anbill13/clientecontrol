package com.example.clientcontrol.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.clientcontrol.R
import com.example.clientcontrol.interfacebutton.ClickListener
import de.hdodenhof.circleimageview.CircleImageView

class IconViewHolder(itemView: View, listener: ClickListener) :
    RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {

    val companyImage: CircleImageView = itemView.findViewById(R.id.iconCompanyIcon)

    private val clickListener = listener

    init {
        itemView.setOnLongClickListener(this)
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        clickListener.onClick(v!!, adapterPosition)
    }

    override fun onLongClick(v: View?): Boolean {

        clickListener.onLongClick(v!!, adapterPosition)
        return true
    }
}