package com.example.clientcontrol.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.clientcontrol.dataclass.*
import com.example.clientcontrol.domain.data.network.StoreRepository

class MainViewModel : ViewModel() {

    private val repository = StoreRepository()

    fun fetCompanyData(): LiveData<MutableList<Company>> {
        val mutableList = MutableLiveData<MutableList<Company>>()
        repository.getCompanies().observeForever {
            mutableList.value = it
        }
        return mutableList
    }

    fun saveCompany(company: CompanyDocument): MutableLiveData<Boolean> {
        return repository.saveCompany(company)
    }


    fun updateCompany(
        companyId: String,
        company: CompanyDocument
    ): LiveData<MutableList<CompanyDocument>> {
        val companyMutableList = MutableLiveData<MutableList<CompanyDocument>>()
        repository.updateCompany(companyId, company).observeForever {
            companyMutableList.value = it
        }
        return companyMutableList
    }


    fun deleteCompany(companyId: String): MutableLiveData<Boolean> {
        return repository.deleteCompany(companyId)

    }

    fun saveClient(companyId: String, client: ClientDocument): MutableLiveData<Boolean> {
        return repository.saveClient(companyId, client)
    }


    fun fetchClientData(companyId: String): LiveData<MutableList<Client>> {
        val clientMutableList = MutableLiveData<MutableList<Client>>()
        repository.getClients(companyId).observeForever {
            clientMutableList.value = it
        }

        return clientMutableList
    }

    fun updateClient(
        companyId: String,
        clientId: String,
        clientDocument: ClientDocument
    ): LiveData<MutableList<ClientDocument>> {
        val clientMutableList = MutableLiveData<MutableList<ClientDocument>>()
        repository.updateClient(companyId, clientId, clientDocument).observeForever {
            clientMutableList.value = it
        }
        return clientMutableList
    }

    fun deleteClient(companyId: String, clientId: String): MutableLiveData<Boolean> {
        return repository.deleteClient(companyId, clientId)
    }

    fun fetchDirectionsData(clientId: String): LiveData<MutableList<Direction>> {
        return repository.getDirections(clientId)
    }

    fun saveDirectionsData(
        companyId: String,
        clientId: String,
        direction: MutableList<Direction>
    ): MutableLiveData<Boolean> {
        return repository.saveDirectionsDetails(companyId, clientId, direction)
    }

}